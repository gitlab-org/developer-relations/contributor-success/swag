This project is used to award wider community members with prizes as a thank you for their contributions.

Prizes can be coupons for to redeem in our
[GitLab community swag store](https://gitlabcommunity.orderpromos.com/redemption_codes/new),
or the option to plant tree for an equivalent value.

Each prize will be sent by creating a confidential issue assigned to the recipient.

The issue will identify the prize "tier" and ask if the recipient would like to redeem it as swag,
or have trees planted.
A due date will be set for 1-month after which any unclaimed rewards will be automatically opted in
to have trees planted.
There will be a window of 1-week after for any last minute responses, before the order is placed.

Labels will be used to track details such as:

- Event.
- Prize tier.
- Selection.

This should allow us report on various details.

We will look to automate this progressively.
